/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TestCars {
     public static void main(String[] args) {
        Cars car=new Cars("car","model","engine","color",0,0);
        car.speak();
        
        Nissan GTR=new Nissan("Nissan ","GTR R35 ","3.8L VR38DETT ","White ",592,315);
        GTR.speak();
        System.out.println(GTR.drive());
        System.out.println(GTR.drive(200));
        
        Porsche p911=new Porsche("Porsche ","911","3.8 L Porsche MA1.75/MDG.GA Flat-6 ","Red ",592,315);
        p911.speak();
        System.out.println(p911.drive());
        System.out.println(p911.drive(200));
        
        Mitsubishi evox=new Mitsubishi("Mitsubishi ","Lancer Evolution X","2.0 L 4B11T I4 Turbocharged ","Black ",280,249);
        evox.speak();
        System.out.println(evox.drive());
        System.out.println(evox.drive(200));
     }  
}
